(* Integer Output - binary to ASCII characters.
//
// University of Cambridge Computer Laboratory - Numerical Methods Demos
// (C) DJ Greaves, 2015.
*)

val tens_table = Array.fromList [1,10,100,1000,10000,100000]; (*Table of powers of Tens*)

fun bin2ascii d0 =
   let fun scanup p =    (*find how many digits does our number have in dec*)  
        if Array.sub(tens_table, p) > d0 
            then p-1 
            else scanup(p+1)
        
       val p0 = scanup 0 (*find how many digits did it have from the start*)
       fun digits d0 p =    (*compute digits*) 
           if p<0
             then []             (*zeroth power - no digits*)
             else                 (*some power, need to compute*)
              let val d = d0 div Array.sub(tens_table, p)(* get decimal digit*)
                  val r = d0 - d * Array.sub(tens_table, p)(* and rest*)
              in chr(48 + d) :: digits r (p-1) end
        in digits d0 p0 end
;

bin2ascii 12345
;

fun test (x:int) =
    implode(bin2ascii x);

(* val it = [#"1", #"2", #"3", #"4"]: char list *)


(* eof *)
